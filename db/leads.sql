CREATE SCHEMA IF NOT EXISTS `teste`;
USE `teste` ;

CREATE TABLE IF NOT EXISTS `teste`.`TBL_REGISTROS_LEADS` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `TIPO_PRODUTO` VARCHAR(50) NOT NULL,
  `OBJETIVO_VIDA` VARCHAR(50) NOT NULL,
  `EMAIL` VARCHAR(150) NOT NULL,
  `NOME` VARCHAR(150) NOT NULL,
  `ENDERECO_LOGRADOURO` VARCHAR(250) NOT NULL,
  `ENDERECO_NUMERO` VARCHAR(150) NOT NULL,
  `ENDERECO_COMPLEMENTO` VARCHAR(50) NOT NULL,
  `CIDADE` VARCHAR(150) NOT NULL,
  `ESTADO` VARCHAR(2) NOT NULL,
  `CEP` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
CHARACTER SET latin1 
COLLATE latin1_danish_ci;

INSERT INTO `teste`.`TBL_REGISTROS_LEADS` 
(`TIPO_PRODUTO`,`OBJETIVO_VIDA`, `EMAIL`, `NOME`, `ENDERECO_LOGRADOURO`, `ENDERECO_NUMERO`, `ENDERECO_COMPLEMENTO`, `CIDADE`, `ESTADO`, `CEP`)
VALUES ("previdência", "comprar um carro", "eval@gmail.com", "evandro", "Avenida", "21", "village alvorada", "brasilia", "DF", "73999999");

INSERT INTO `teste`.`TBL_REGISTROS_LEADS` 
(`TIPO_PRODUTO`,`OBJETIVO_VIDA`, `EMAIL`, `NOME`, `ENDERECO_LOGRADOURO`, `ENDERECO_NUMERO`, `ENDERECO_COMPLEMENTO`, `CIDADE`, `ESTADO`, `CEP`)
VALUES ("previdência", "juntar dinheiro", "antonio@hotmail.com", "antonio", "Avenida", "21", "teste2", "rio de janeiro", "RJ", "73999999");

INSERT INTO `teste`.`TBL_REGISTROS_LEADS` 
(`TIPO_PRODUTO`,`OBJETIVO_VIDA`, `EMAIL`, `NOME`, `ENDERECO_LOGRADOURO`, `ENDERECO_NUMERO`, `ENDERECO_COMPLEMENTO`, `CIDADE`, `ESTADO`, `CEP`)
VALUES ("previdência", "comprar um imóvel", "antonio@hotmail.com", "antonio", "Avenida", "12", "teste23", "brasilia", "DF", "73999999");

INSERT INTO `teste`.`TBL_REGISTROS_LEADS` 
(`TIPO_PRODUTO`,`OBJETIVO_VIDA`, `EMAIL`, `NOME`, `ENDERECO_LOGRADOURO`, `ENDERECO_NUMERO`, `ENDERECO_COMPLEMENTO`, `CIDADE`, `ESTADO`, `CEP`)
VALUES ("previdência", "juntar dinheiro", "let@gmail.com", "leticia", "Avenida", "23", "teste1", "belem", "PA", "73999999");

INSERT INTO `teste`.`TBL_REGISTROS_LEADS` 
(`TIPO_PRODUTO`,`OBJETIVO_VIDA`, `EMAIL`, `NOME`, `ENDERECO_LOGRADOURO`, `ENDERECO_NUMERO`, `ENDERECO_COMPLEMENTO`, `CIDADE`, `ESTADO`, `CEP`)
VALUES ("previdência", "comprar um imóvel", "joao@gmail.com", "João", "Avenida", "222", "testezzxx", "sao paulo", "SP", "73999999");
