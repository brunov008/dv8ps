import { IsNotEmpty, IsString } from 'class-validator';

export class Produto {
    @IsNotEmpty()
    @IsString()
    readonly nome: string;

    //@IsNotEmpty()
    //@IsString()
    readonly objetivo: string;
  }