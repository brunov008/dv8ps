import { IsNotEmpty, IsString } from 'class-validator';

export class Endereco {
    @IsNotEmpty()
    @IsString()
    readonly logradouro: string;

    @IsNotEmpty()
    @IsString()
    readonly numero: string;

    @IsNotEmpty()
    @IsString()
    readonly complemento: string;

    @IsNotEmpty()
    @IsString()
    readonly cidade: string;

    @IsNotEmpty()
    @IsString()
    readonly estado: string;

    @IsNotEmpty()
    @IsString()
    readonly cep: string;
  }