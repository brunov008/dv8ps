import { Type } from 'class-transformer';
import { Produto } from "./Produto.model";
import { Cliente } from "./Cliente.model";
import { IsNotEmptyObject, IsObject, ValidateNested } from 'class-validator';

export class Cadastro {
    @IsNotEmptyObject()
    @IsObject()
    @ValidateNested()
    @Type(() => Produto)
    readonly produto: Produto

    @IsNotEmptyObject()
    @IsObject()
    @ValidateNested()
    @Type(() => Produto)
    readonly cliente: Cliente
  }