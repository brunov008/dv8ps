import { IsNotEmpty, IsString, IsEmail} from 'class-validator';
import { Type } from 'class-transformer';
import { Endereco } from "./Endereco.model";
import { IsNotEmptyObject, IsObject, ValidateNested } from 'class-validator';

export class Cliente {
    @IsNotEmpty()
    @IsString()
    readonly nome: string

    @IsNotEmpty()
    @IsEmail()
    readonly email: string
    
    @IsNotEmptyObject()
    @IsObject()
    @ValidateNested()
    @Type(() => Endereco)
    readonly endereco: Endereco
  }