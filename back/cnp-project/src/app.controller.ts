import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { Cadastro } from './models/Cadastro.model';
import { RegistroLeads } from './entities/registro.entity';

@Controller('leads')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async listar(): Promise<RegistroLeads[]> {
    const list =  await this.appService.listar();

    return list.reverse();
  }

  @Get(':id')
  async getOne(@Param('id') id : number): Promise<RegistroLeads>{
    return await this.appService.getOne(id)
  }

  @Post()
  async cadastrar(@Body() body: Cadastro): Promise<Object> {

    try{
      await this.appService.cadastrar(body);
    }catch(e){
      throw new Error('Ocorreu um erro ao adicionar no banco')
    }
    return { message: 'Cadastro realizado com sucesso.' }
  }
}
