import { Injectable } from '@nestjs/common';
import { Cadastro } from './models/Cadastro.model';
import { RegistroLeads} from './entities/registro.entity'
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class AppService { 

  constructor(
    @InjectRepository(RegistroLeads) readonly registroRepository: Repository<RegistroLeads>,
  ){}
  
  async listar(): Promise<RegistroLeads[]> {
    return this.registroRepository.find()
  }

  async getOne(id): Promise<RegistroLeads>{
    return this.registroRepository.findOne({
      where: {
        ID: id
      }
    })
  }

  async cadastrar(model : Cadastro): Promise<void> {

    const entity = new RegistroLeads() 
    entity.CEP = model.cliente.endereco.cep
    entity.CIDADE = model.cliente.endereco.cidade
    entity.EMAIL = model.cliente.email
    entity.ENDERECO_COMPLEMENTO = model.cliente.endereco.complemento
    entity.ENDERECO_LOGRADOURO = model.cliente.endereco.logradouro
    entity.ENDERECO_NUMERO = model.cliente.endereco.numero
    entity.ESTADO = model.cliente.endereco.estado
    entity.NOME = model.cliente.nome
    entity.OBJETIVO_VIDA = model.produto.objetivo
    entity.TIPO_PRODUTO = model.produto.nome

    await this.registroRepository.save(entity)
  }
}
