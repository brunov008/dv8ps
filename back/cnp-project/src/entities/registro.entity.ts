import {Entity, PrimaryGeneratedColumn, Column} from 'typeorm';

@Entity({ name: "TBL_REGISTROS_LEADS" })
export class RegistroLeads {
    @PrimaryGeneratedColumn()
    ID: number;

    @Column()
    TIPO_PRODUTO: string;
    
    @Column()
    OBJETIVO_VIDA: string;

    @Column()
    EMAIL: string;

    @Column()
    NOME: string;

    @Column()
    ENDERECO_LOGRADOURO: string;

    @Column()
    ENDERECO_NUMERO: string;

    @Column()
    ENDERECO_COMPLEMENTO: string;

    @Column()
    CIDADE: string;

    @Column()
    ESTADO: string;

    @Column()
    CEP: string;
}