## Tools
- Docker
- Nest.JS
- Angular
- REST API


## Pre Requisites
- Docker: [https://www.docker.com]

## Running the app
- Verifique se o docker esta disponivel nas variaveis com o comando [docker -v] e [docker-compose -v]
- inicie com o comando [docker-compose up] na raiz do projeto 

## Steps
- As images serão adicionadas diretamente do DockerHub [https://hub.docker.com]
- Serão iniciados os containers de banco, back-end e front-end respectivamente
- Ao final, acesse o endereço [http://localhost]

Obs: O container angular pode demorar um pouco para iniciar (Irei melhorar em breve)


