import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InformationComponent } from './components/information/information.component';
import { ListComponent } from './components/list/list.component';
import { LoginComponent } from './components/login/login.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/login",
    pathMatch: "full"
  }, 
  {
    path: "list",
    component: ListComponent,
    canActivate: [AuthGuard]
  }, {
    path: 'form',
    component: FormularioComponent,
    canActivate: [AuthGuard]
  },{
    path: 'list/:id',
    component: InformationComponent,
    canActivate: [AuthGuard]
  },{ 
    path: 'login',
    component: LoginComponent
  },
  { 
    path: '**',
    redirectTo: "/login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
