import { Injectable } from '@angular/core';

import { Formulario } from '../models/formulario.models';
import { BaseResponse } from '../models/base.response.modes';
import { User } from '../models/user.models';
import { PeriodicElement } from '../models/element.models';
import {CustomHttpRequest, EndPoint, TYPE} from 'src/app/utils/http.request.utils';
import { BehaviorSubject } from 'rxjs';
const {POST,DELETE, PUT} = TYPE;
const {LEADS, AUTENTICAR} = EndPoint;

@Injectable({
  providedIn: 'root'
})
export class MainService {

  loggedIn = new BehaviorSubject<boolean>(false)

  get isLoggedIn() {
    return this.loggedIn.asObservable()
  }

  logout = () => this.loggedIn.next(false)

  insertForm = (form : Formulario) => CustomHttpRequest.request<BaseResponse>(LEADS, POST, form)

  login = (user: User) => CustomHttpRequest.request<BaseResponse>(AUTENTICAR, POST, user)

  getList = () => CustomHttpRequest.request<PeriodicElement[]>(LEADS)

  getOne = (id: number) => CustomHttpRequest.request<PeriodicElement>(LEADS + `/${id}`)
}
