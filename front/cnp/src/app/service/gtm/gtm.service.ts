import { Injectable } from '@angular/core';
import { WindowRefService, ICustomWindow } from 'src/app/service/gtm/window.ref.service';

@Injectable({
  providedIn: 'root'
})
export class GTMService {

  private _window: ICustomWindow;

  constructor(windowRef: WindowRefService) {
    this._window = windowRef.nativeWindow;
   }

   triggerSuccessLogin(){
    this._window.dataLayer.push({
      "event": "loginFormulario"
    })
   }

   triggerFailureLogin(){
    this._window.dataLayer.push({
      "event": "loginError"
    })
   }
}
