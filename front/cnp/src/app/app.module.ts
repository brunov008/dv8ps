import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MainService } from './service/main-service.service';
import { InterceptorModule } from './modules/interceptor/interceptor.module';
import { LoginComponent } from './components/login/login.component';
import { InformationComponent } from './components/information/information.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { ListComponent } from './components/list/list.component';
import { WindowRefService } from './service/gtm/window.ref.service';
import { GTMService } from './service/gtm/gtm.service';
import { DialogComponent } from './shared/dialog/dialog.component';
import { SnackbarComponent } from './shared/snackbar/snackbar.component';
import { NavbarComponent } from './shared/navbar/navbar.component';

import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog'; 
import { MatSnackBarModule } from '@angular/material/snack-bar'; 
import { CdkTableModule } from '@angular/cdk/table';
import {MatSidenavModule} from '@angular/material/sidenav';

@NgModule({
  declarations: [
    AppComponent,
    InformationComponent,
    LoginComponent,
    FormularioComponent,
    ListComponent,
    DialogComponent,
    SnackbarComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    InterceptorModule,
    BrowserAnimationsModule,
    MatSortModule,
    MatPaginatorModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatMenuModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatToolbarModule,
    CdkTableModule,
    MatBottomSheetModule,
    MatSidenavModule,
  ],
  providers: [MainService, WindowRefService, GTMService],
  bootstrap: [AppComponent]
})
export class AppModule { }
