import { Component, Inject } from '@angular/core';
import { PeriodicElement } from 'src/app/models/element.models';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent {

  element : PeriodicElement = null

  constructor(
    @Inject(MAT_DIALOG_DATA) data: any, 
    private _dialogRef:MatDialogRef<DialogComponent>) {
      this.element = data.element
   }
}
