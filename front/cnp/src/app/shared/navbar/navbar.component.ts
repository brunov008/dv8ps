import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/service/main-service.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  constructor(
    private mainService: MainService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isLoggedIn$ = this.mainService.isLoggedIn;
  }

  onLogout() : void{
    this.mainService.logout();
    this.router.navigate(['/login'])
  }

}
