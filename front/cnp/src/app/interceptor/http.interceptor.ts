import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor {

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    
    request = request.clone({
      setHeaders: {
        'Token': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwMzUwMDcwNmMyZTZiMDA xYWUwMmM0OSIsImVtYWlsIjoidGVzdGVAdGVzdGUuY29tLmJyIiwibmFtZSI6InRl c3RlIiwiaWF0IjoxNjE0ODI4OTk1LCJleHAiOjMyMjk3NDQzOTB9.0cT7ArGb72UNFZ QRBbEjdBm_2GcO8jdoHXQmvtOEIXw',
        'Content-Type': 'application/json'
      }
    })

    return next.handle(request);
  }
}
