export interface Formulario{
    produto: Produto
    cliente: Cliente
}

export interface Produto{
    nome: String;
    objetivo: String;
}

export interface Cliente{
    nome: String,
    email: String,
    endereco : Endereco

}

export interface Endereco{
    logradouro: String,
    numero: String,
    complemento: String,
    cidade: String,
    estado: String,
    cep: String
}