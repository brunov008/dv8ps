export interface PeriodicElement {

    ID: string;
    TIPO_PRODUTO: string;
    OBJETIVO_VIDA: string;
    EMAIL: string;
    NOME: string;
    ENDERECO_LOGRADOURO: string;
    ENDERECO_NUMERO: string;
    ENDERECO_COMPLEMENTO: string;
    CIDADE: string;
    ESTADO: string;
    CEP: string;
  }