import { HttpClient, HttpXhrBackend} from '@angular/common/http';
import {environment} from 'src/environments/environment'
import { Observable } from 'rxjs';

export class CustomHttpRequest{

    private static http : HttpClient = new HttpClient(new HttpXhrBackend({ build: () => new XMLHttpRequest()}))

    static request<T> (endpoint: EndPoint | String, method?: TYPE, body?: Object) : Observable<T>{
        const url = environment.api + endpoint

        switch(method){
            case TYPE.GET: return this.http.get<T>(url)
            case TYPE.POST: return this.http.post<T>(url, body)

            default : return this.http.get<T>(url)
        }
    }
}

export enum EndPoint {
    LEADS = '/leads',
    AUTENTICAR = '/autenticar'
  }

export enum TYPE {
    GET,
    POST,
    PUT,
    DELETE
}