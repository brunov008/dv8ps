import { Observable } from 'rxjs';
import {DataSource} from '@angular/cdk/collections';

export class ExampleDataSource<T> extends DataSource<T> {

    constructor(private data: T[]){
      super();
    }

    connect(): Observable<T[]> {
      return new Observable(sub => sub.next(this.data))
    }
    
    disconnect() {
    }
  }