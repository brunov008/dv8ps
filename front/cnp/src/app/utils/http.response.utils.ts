import { HttpErrorResponse } from '@angular/common/http'
import { GTMService } from '../service/gtm/gtm.service'

export class HttpError{
  
    static error = (gtm?: GTMService) => (e : HttpErrorResponse) => {
        if(gtm) gtm.triggerFailureLogin()

        if(e.status >= 400 && e.status <= 499){
          alert('Campos incorretos: '+ e.error.message)
        }else if(e.status === 0){
          alert('Sem comunicacao com o servidor')
        }else if(e.status >= 500){
          alert('Erro do servidor')
        }
      }
}