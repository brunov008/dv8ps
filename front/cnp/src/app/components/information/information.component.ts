import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MainService } from 'src/app/service/main-service.service';
import { HttpError } from 'src/app/utils/http.response.utils';
import { PeriodicElement } from 'src/app/models/element.models';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { DialogComponent } from 'src/app/shared/dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

  private id : number
  user : PeriodicElement = null

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MainService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = Number(params.get('id'))
      this.getUserInfo()
    })
  }

  getUserInfo(){
    this.service.getOne(this.id).subscribe(data => this.user = data, HttpError.error)
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        element: this.user
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    })

  }
}
