import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import {  FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { MainService } from 'src/app/service/main-service.service';
import { Formulario } from 'src/app/models/formulario.models';
import { BaseResponse } from 'src/app/models/base.response.modes';
import { HttpError } from 'src/app/utils/http.response.utils';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit, AfterViewInit {

  //Todo validacao
  checkoutForm = new FormGroup({
    selObjetivo: new FormControl(null),
    txtNome: new FormControl(null, [Validators.required]),
    txtEmail: new FormControl(null),
    txtCEP: new FormControl(null),
    txtLogradouro: new FormControl(null),
    txtNumero: new FormControl(null),
    txtComplemento: new FormControl(null),
    txtCidade: new FormControl(null),
    selEstado: new FormControl(null),
  });

  objetivos = ['Comprar um carro', 'Juntar dinheiro', 'Comprar um imóvel']
  estados = ["AC","AL","AP","AM","BA","CE","DF","ES","GO","MA","MS","MT","MG","PA","PB","PR","PE","PI","RJ","RN","RS","RO","RR","SC","SP","SE","TO"]
  
  constructor(
    private router: Router,
    private service: MainService,
  ){}

  //Ciclo de vida
  ngOnInit(){
    
  }
  
  //Ciclo de vida
  ngAfterViewInit(){
    
  }

  submit(){
    const {selObjetivo, txtNome, txtEmail, txtCEP, txtLogradouro, txtNumero, txtComplemento, 
      txtCidade, selEstado} = this.checkoutForm.value

    const data : Formulario = {
      produto: {
        nome: 'previdência', //TODO
        objetivo: selObjetivo
      },
      cliente: {
        nome: txtNome,
        email: txtEmail,
        endereco: {
          cep: txtCEP,
          cidade: txtCidade,
          complemento: txtComplemento,
          estado: selEstado,
          logradouro: txtLogradouro,
          numero: txtNumero
        }
      }
    } 

    this.service.insertForm(data).subscribe((data : BaseResponse) => {
      /*
      this.dataLayer.push({
        'clienteForm': 'ok',
      })
      */
      alert(data.message)
      this.checkoutForm.reset()
    }, HttpError.error)
  }

}
