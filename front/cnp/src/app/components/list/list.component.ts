import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MainService } from 'src/app/service/main-service.service';
import { HttpError } from 'src/app/utils/http.response.utils';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['EMAIL', 'NOME']
  dataSource = []

  constructor(
    private router: Router,
    private service: MainService
  ) { } 

  //Ciclo de Vida
  ngOnInit(){
    this.getList()
  }

  //Ciclo de Vida
  ngAfterViewInit(){
    
  }

  getList() {
    this.service.getList().subscribe(
      data =>  this.dataSource = data,
      HttpError.error
    );
  }

  displayData(id){
    this.router.navigate([`/list/${id}`])
  }
}