import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MainService } from 'src/app/service/main-service.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.models';
import { HttpError } from 'src/app/utils/http.response.utils';
import { GTMService } from 'src/app/service/gtm/gtm.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  checkoutForm = new FormGroup({
    email: new FormControl(null),
    senha: new FormControl(null),
    
  });

  constructor(
    private router: Router,
    private service: MainService,
    private gtm: GTMService
  ) {}

  //Ciclo de vida
  ngOnInit(){

  }

  submit(){
    const {email, senha} = this.checkoutForm.value

    const data : User = {
      email: email,
      senha: senha
    }

    this.service.login(data).subscribe(
      data => {
        if(data.message === 'ok'){
          this.service.loggedIn.next(true)
          this.gtm.triggerSuccessLogin()
          this.router.navigate(['/list'])
        }else{
          this.gtm.triggerFailureLogin()
          alert('Tente novamente')
        }
      }, 
      HttpError.error(this.gtm))
  }

}
